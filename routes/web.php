<?php

use Illuminate\Support\Facades\Route;

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::get('/', function () {
    return view('welcome');
});


Route::get('menu', 'CUPSController@index');


Route::get('Expresso', 'CUPSController@Expresso');

Route::get('Americano', 'CUPSController@Americano');
Route::get('Cuppochino', 'CUPSController@Cuppochino');
Route::get('Hotcoffees', 'CUPSController@Hotcoffees');
Route::get('Iced', 'CUPSController@Iced');
Route::get('Icy', 'CUPSController@Icy');
Route::get('Vanilla', 'CUPSController@Vanilla');
Route::get('Chocolate', 'CUPSController@Chocolate');

Route::get('aboutus', 'CUPSController@aboutus');

Route::get('shop', 'CUPSController@shop');

Route::get('user-registration', 'UserController@index');

Route::post('user-store', 'UserController@userPostRegistration');

Route::get('user-login', 'UserController@userLoginIndex');

Route::post('login', 'UserController@userPostLogin');

Route::get('dashboard', 'UserController@dashboard');

Route::get('logout', 'UserController@logout');

Route::get('upload', 'ImageUploadController@index');

Route::post('upload-images/', 'ImageUploadController@uploadImages');