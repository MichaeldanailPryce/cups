@extends('layout.master')

@section('content')   <style>
            html, body {
                background-color: #fff;
                color: #636b6f;
                font-family: 'Nunito', sans-serif;
                font-weight: 200;
                height: 100vh;
                margin: 0;
            }

           /* .bg-img {
  /* The image used */
  /*background-image: url("img/coffee3.jpg");

  min-height: 680px;
  /* Center and scale the image nicely */
/* background-position: center;
  background-repeat: no-repeat;
  background-size: cover;

  /* Needed to position the navbar */
  /*position: relative;
}

/* Position the navbar container inside the image */
/*.container {
  /*position: absolute;
  margin: 20px;
  width: auto;
}

/* The navbar */
.topnav {
  overflow: hidden;
  background-color: #333;
}

/* Navbar links */
.topnav a {
  float: left;
  color: #f2f2f2;
  text-align: center;
  padding: 14px 16px;
  text-decoration: none;
  font-size: 23px;
}

.topnav a:hover {
  background-color: #ddd;
  color: black;
}
.topnav input[type=text] {
  float: right;
  padding: 6px;
  border: none;
  margin-top: 8px;
  margin-right: 16px;
  font-size: 17px;
}
/* When the screen is less than 600px wide, stack the links and the search field vertically instead of horizontally */
@media screen and (max-width: 600px) {
  .topnav a, .topnav input[type=text] {
    float: none;
    display: block;
    text-align: left;
    width: 100%;
    margin: 0;
    padding: 14px;
  }
  .topnav input[type=text] {
    border: 1px solid #ccc;
  }

  .topnav a, .topnav input[type=text], .topnav .search-container button {
    float: none;
    display: block;
    text-align: left;
    width: 100%;
    margin: 0;
    padding: 14px;
  }
 
}
.logo{
 float: left;
 padding:0 18px;
 font-size: 40px;
 background-color: #ddd;
  color: black;
}

 </style>
    </head>
    <body>
        <div class="flex-center position-ref full-height">
            @if (Route::has('login'))
                <div class="top-right links">
                    @auth
                        <a href="{{ url('/home') }}">Home</a>
                    @else
                        <a href="{{ route('login') }}">Login</a>

                        @if (Route::has('register'))
                            <a href="{{ route('register') }}">Register</a>
                        @endif
                    @endauth
                </div>
            @endif
        
            <div class="container">
            <div class="logo">
              CUPS

            </div>
    <div class="topnav">
      <a href="/">HOME</a>
      <a href="/menu">MENU</a>
      <a href="/aboutus">ABOUT US</a>
    
      <input type="text" placeholder="Search..">
     
    </div>
</div>
  </div>
</div><br><br>

<div class="container mt-3">

    <div class="row">

        <div class="col-xl-6 col-lg-6 col-md-6 col-sm-12 col-12 m-auto">

        <form method="post" action="{{ url('login') }}">

                <div class="card shadow">

                    <div class="car-header bg-success pt-2">

                        <div class="card-title font-weight-bold text-white text-center"> User Login </div>

                    </div>



                    <div class="card-body">

                            @if(Session::has('error'))

                                <div class="alert alert-danger">

                                    {{ Session::get('error') }}

                                    @php

                                        Session::forget('error');

                                    @endphp

                                </div>

                            @endif





                        <div class="form-group">

                            <label for="email"> E-mail </label>

                            <input type="text" name="email" id="email" class="form-control" placeholder="Enter E-mail" value="{{ old('email') }}"/>

                            {!! $errors->first('email', '<small class="text-danger">:message</small>') !!}

                        </div>



                        <div class="form-group">

                            <label for="password"> Password </label>

                            <input type="password" name="password" id="password" class="form-control" placeholder="Enter Password" value="{{ old('password') }}"/>

                            {!! $errors->first('password', '<small class="text-danger">:message</small>') !!}

                        </div>

                    </div>



                    <div class="card-footer d-inline-block">

                        <button type="submit" class="btn btn-success"> Login </button>

                        <p class="float-right mt-2"> Don't have an account?  <a href="{{ url('user-registration')}}" class="text-success"> Register </a> </p>
                        <p class="float-left mt-2"> Do want to upload a photo  <a href="{{ url('upload')}}" class="text-success"> upload </a> </p>

                    </div>

                    @csrf

                </div>

            </form>

        </div>

    </div>
    </div>
@endsection
