

@extends('layout.menu')
<style>
* {
  box-sizing: border-box;
}

.column {
  float: left;
  width: 33.33%;
  padding: 50px;
}

/* Clearfix (clear floats) */
.row::after {
  content: "";
  clear: both;
  display: table;
}

button{
  width: 100%;
  margin-top: 10px;
  padding: 10px;
  border: none;
  border-radius: 30px;
  background: #52ad9c;
  color: #fff;
  font-size: 15px;
  font-weight: bold;
}
button:hover{
  cursor: pointer;
  background: #428a7d;
}

</style>
@section('content')

<div class="row">
  <div class="column">
    <img src="img/am.jpg" style="width:350px;height:200px"/>
	<p>Americano</p>
	<form action="/Americano">
         <button type="submit">View Product</button>
      </form>
  </div>
  <div class="column">
    <img src="img/dark.jpg" style="width:350px;height:200px"/>
	   <p>Dark Chocolate Mocha</p>
	   <form action="/Chocolate">
         <button type="submit">View Product</button>
      </form>
  </div>
  <div class="column">
    <img src="img/ice latte.jpg" style="width:350px;height:200px"/>
	   <p>Iced Latte</p>
	   <form action="/Iced">
         <button type="submit">View Product</button>
      </form>
  </div>
  <div class="column">
    <img src="img/vanilla.jpg" style="width:350px;height:200px"/>
	<p> Vanilla Latte</p>
	<form action="/Vanilla">
         <button type="submit">View Product</button>
      </form>
  </div>
  <div class="column">
    <img src="img/cap.jpg" style="width:350px;height:200px"/>
	  <p> Cuppochino</p>
	  <form action="/Cuppochino">
         <button type="submit">View Product</button>
      </form>
  </div>
  <div class="column">
    <img src="img/lavender.jpg" style="width:350px;height:200px"/>
	<p>Icy Lavender Latte</p>
	<form action="/Icy">
         <button type="submit">View Product</button>
      </form>
  </div>
</div>
@endsection