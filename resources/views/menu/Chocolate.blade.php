<!DOCTYPE html>
<html lang="en">
<head> 

<style>
* {
  box-sizing: border-box;
}

.column {
  float: left;
  width: 33.33%;
  padding: 50px;
}

/* Clearfix (clear floats) */
.row::after {
  content: "";
  clear: both;
  display: table;
}

button{
  width: 100%;
  margin-top: 10px;
  padding: 10px;
  border: none;
  border-radius: 30px;
  background: #52ad9c;
  color: #fff;
  font-size: 15px;
  font-weight: bold;
}
button:hover{
  cursor: pointer;
  background: #428a7d;
}
</style>
</head>

<body>

<div class="row">
  <div class="column">
    <img src="img/dark.jpg" width="400px" height="400px"

style= "margin-right: 10px; float: left; "/>
	</div>
	   <div class="column">
	   <header>
	   
	 <h1>Dark Chocolate Mocha</h1>
	   
	   <p><font size="5">Meet our new signature beverage: 
	   Rich espresso and freshly steamed milk meet Dutch cocoa powder and 
	   house-made chocolate sauce to create a more intense chocolate flavor, 
	   topped with a blanket of fresh whipped cream and a dusting of cocoa powder.</font></p>
  
 
	   <form action="Dark Chocolate Mocha.html">
         <button type="submit">Order</button>
      </form>
	  
	   </div>
	   
	</div>
	

</header>

</body>

</html>