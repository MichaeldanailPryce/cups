 <!DOCTYPE html>
<html lang="en">
<head> 

<style>
* {
  box-sizing: border-box;
}

.column {
  float: left;
  width: 33.33%;
  padding: 50px;
}

/* Clearfix (clear floats) */
.row::after {
  content: "";
  clear: both;
  display: table;
}

button{
  width: 100%;
  margin-top: 10px;
  padding: 10px;
  border: none;
  border-radius: 30px;
  background: #52ad9c;
  color: #fff;
  font-size: 15px;
  font-weight: bold;
}
button:hover{
  cursor: pointer;
  background: #428a7d;
}

</style>
</head>

<body>

<div class="row">
  <div class="column">
  
  <img src="img/cap.jpg" width="400px" height="400px"

style= "margin-right: 10px; float: left; "/>

	</div>
	   <div class="column">
	   <header>
	   <h1>Cuppochino</h1>
	
	   <p><font size="5">This coffee-forward classic has just two ingredients, 
	   but requires the utmost barista skill. We hand-pull a perfect espresso and marble 
	   it with creamy microfoam (thick steamed milk), 
	   stretching the espresso's caramel notes and providing a smooth texture.</font></p>
  
 
	   <form action="Cuppochino.html">
         <button type="submit">Order</button>
      </form>
	  
	   </div>
	   
	</div>
	


</header>
</body>

</html>