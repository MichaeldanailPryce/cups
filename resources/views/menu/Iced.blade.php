<!DOCTYPE html>
<html lang="en">
<head> 

<style>
* {
  box-sizing: border-box;
}

.column {
  float: left;
  width: 33.33%;
  padding: 50px;
}

/* Clearfix (clear floats) */
.row::after {
  content: "";
  clear: both;
  display: table;
}

button{
  width: 100%;
  margin-top: 10px;
  padding: 10px;
  border: none;
  border-radius: 30px;
  background: #52ad9c;
  color: #fff;
  font-size: 15px;
  font-weight: bold;
}
button:hover{
  cursor: pointer;
  background: #428a7d;
}

</style>
</head>

<body>

<div class="row">
  <div class="column">
  
  <img src="img/ice latte.jpg" width="400px" height="400px"

style= "margin-right: 10px; float: left; "/>

	</div>
	   <div class="column">
	   <header>
	   <h1>Iced Latte</h1>
	
	   <p><font size="5">Everything we love about a hot Caffe Latte, at cooler temperatures. 
	   We start with a layer of foam, followed by a little ice. 
	   When cold milk and rich Espresso Forte is carefully poured in, 
	   the foam rises through the ice to meet them, delivering a creamy texture.</font></p>
  
 
	   <form action="Iced Latte.html">
         <button type="submit">Order</button>
      </form>

	  
	   </div>
	   
	</div>
	


</header>
</body>

</html>
