<!DOCTYPE HTML>
<html lang="en">
<head>
<style>
* {
  box-sizing: border-box;
}

.column {
  float: left;
  width: 33.33%;
  padding: 50px;
}

/* Clearfix (clear floats) */
.row::after {
  content: "";
  clear: both;
  display: table;
}

button{
  width: 100%;
  margin-top: 10px;
  padding: 10px;
  border: none;
  border-radius: 30px;
  background: #52ad9c;
  color: #fff;
  font-size: 15px;
  font-weight: bold;
}
button:hover{
  cursor: pointer;
  background: #428a7d;
}

</style
 </head> 
<body>
<div class="row">

<div class="column">

<img src="img/am.jpg" style="margin-right: 10px; float: left; " />


	 </div>
	 
<div class="column">

<header> 
            <h1> Americano</h1> 	   
             
	   	    
<p><font size="5"> An Americano is two ingredients: espresso and hot water.    
When rich, hand-pulled Espresso Forte is poured into hot water,   you retain the 
perfect crema in a pleasantly sippable cup with notes of malty 
hazelnut.</font></p>

<form action="Americano.html"><button type="submit">Order</button>       
</form<
</div>
</div>
</header>
</body>
</html>
